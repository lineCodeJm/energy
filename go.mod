module github.com/energye/energy

go 1.18

require (
	github.com/energye/golcl v0.0.0-20230202025428-f55a3e161f4a
	github.com/godbus/dbus/v5 v5.1.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/tevino/abool v0.0.0-20220530134649-2bfc934cb23c
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f
)
