//----------------------------------------
//
// Copyright © yanghy. All Rights Reserved.
//
// Licensed under GNU General Public License v3.0
//
//----------------------------------------

package cef

import "unsafe"

type ICefBrowserViewDelegate struct {
	instance unsafe.Pointer
}
